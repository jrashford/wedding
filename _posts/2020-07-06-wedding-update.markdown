---
layout: post
title:  "Wedding Update"
date:   2020-07-06 15:32:14 -0300
---
We are very pleased that as the lockdown is gradually easing marriages are now permitted in Wales and our wedding will hopefully be able to take place as planned on September 19th 2020. 

As I am sure you will understand our wedding will be unable to go ahead with the numbers we originally planned.  At present we have no idea how many guests we will be able to have and whether or not the reception venue will still be able to honour our booking in the light of current restrictions. Indeed because the restrictions are constantly changing it is very difficult for us to know exactly what we can plan for.

Based on the current situation we expect that we will have the wedding ceremony with only parents and siblings and possibly close family and friends in attendance.  As yet we have no firm idea what sort of reception we will be able to have, however we very much hope to have something small. We are hoping to be able to live stream the ceremony so that you can still be a part of our wedding. 

We now plan to hold a bigger wedding celebration/reception next year and will send out details regarding this closer to the time when the situation is clearer.

We are sorry not to be able to send you an official invitation just yet. The date is fixed as September 19th. Please keep the date in your diary. We will send out a further update with times and locations when the restrictions at the time become clear. We have created this website in order to do this. 

We would also be very grateful if you would consider buying something from the [wedding list](https://www.amazon.co.uk/wedding/james-ashford-ella-boorman-cardiff-september-2020/registry/2TNKY67MD5Q6T) even if the COVID 19 restrictions mean you are unable to attend our wedding.

Thank you to all those who have been praying for us as we continue to work through the best way forward in this current situation.

Kind regards,

Phil, Sarah, James and Ella